//
//  FortuneViewController.h
//  CrystalBall
//
//  Created by Matthew Berkland on 3/23/13.
//  Copyright (c) 2013 Matthew Berkland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FortuneViewController : UIViewController

- (IBAction)fortunePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *txtFortune;
@property (strong, nonatomic) IBOutlet UIButton *btnFuture;
@end
