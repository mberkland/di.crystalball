//
//  AppDelegate.m
//  CrystalBall
//
//  Created by Matthew Berkland on 3/23/13.
//  Copyright (c) 2013 Matthew Berkland. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize  fortune;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    //initlize the foruten view controller
    fortune = [[FortuneViewController alloc]init];
    
    //set up the root view
    self.window.rootViewController = fortune;
    
    //carry on....    
    [self.window makeKeyAndVisible];
    return YES;
}


@end
