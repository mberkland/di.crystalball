//
//  FortuneViewController.m
//  CrystalBall
//
//  Created by Matthew Berkland on 3/23/13.
//  Copyright (c) 2013 Matthew Berkland. All rights reserved.
//

#import "FortuneViewController.h"

@interface FortuneViewController ()

@end

NSArray *fortunes;
int fortunesGiven=0;

@implementation FortuneViewController

@synthesize txtFortune;
@synthesize btnFuture;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //populate our random fortunes here
    [self initFortunes];
    
    //check to see if we are over on fortunes when the app start
    BOOL over = [[NSUserDefaults standardUserDefaults] boolForKey:@"overage"];
    if(over)
    {
        //we are over!
        //Nag and hide the fortune button....
        [self nag];
        btnFuture.hidden = YES;
        
    }
}

-(void) initFortunes{
    fortunes = [[NSArray alloc]
                    initWithObjects:
                @"Maybe",
                @"Doesn't look good",
                @"Probably not",
                @"Better luck next time",
                @"No way",
                @"Heck no",
                @"Hahahahaha",
                nil];


}

-(void) nag
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"No more" message:@"You only get 3 free fortunes, you should upgrade" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [av show];
    
}

-(NSString*) pickAFortune{
    //implement some business logic, you only get 3 fortunes for free
    if(fortunesGiven >= 3)
    {
        //over our limit, show the nag alert
        [self nag];
        
        //store in NSUserdefaults that we've over run our free fortunes
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"overage"];
        
        return @"...";
    }
    else
    {
        fortunesGiven++;
        //create a random number between 0 and 3;
        int index = arc4random_uniform([fortunes count]);
    
        //pull the first one out
        NSString *fortune = [fortunes objectAtIndex:index];
    
        return  fortune;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)fortunePressed:(id)sender {
    //simplified...
    self.txtFortune.text = [self pickAFortune];
}
@end
