//
//  AppDelegate.h
//  CrystalBall
//
//  Created by Matthew Berkland on 3/23/13.
//  Copyright (c) 2013 Matthew Berkland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FortuneViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) FortuneViewController *fortune;

@end
